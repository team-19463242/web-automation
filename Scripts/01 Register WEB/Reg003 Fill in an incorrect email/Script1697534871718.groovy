import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/daftar?')

WebUI.setText(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/input_Nama_name'), 
    'Eki')

WebUI.sendKeys(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/input_E-Mail_email'), 
    'eki.com')

WebUI.setText(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/input_Whatsapp_whatsapp'), 
    '081395432167')

WebUI.setEncryptedText(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/input_Kata Sandi_password'), 
    'KoKdG/X7DWY0qkXw1/G3hg==')

WebUI.setEncryptedText(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_password_confirmation'), 
    'KoKdG/X7DWY0qkXw1/G3hg==')

WebUI.click(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_inlineCheckbox1'))

WebUI.click(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/button_Daftar'))

WebUI.verifyElementText(findTestObject('Object Repository/Register WEB/Reg003 Fill in an incorrect email/Page_Buat akun dan dapatkan akses di Coding.ID/span_Buat Akun Baru'), 
    'Buat Akun Baru')

WebUI.takeScreenshot()

WebUI.closeBrowser()

