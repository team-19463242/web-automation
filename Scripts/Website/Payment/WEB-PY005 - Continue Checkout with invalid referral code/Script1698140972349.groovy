import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.callTestCase(findTestCase('Website/Common Test Case/Login'), [('Email') : 'fatija3142@locawin.com', ('Password') : 'cZFrDSk31FeaspcjiMwZ6g=='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Website/Common Test Case/Add Event to Cart'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.check(findTestObject('Object Repository/Website/Checkout Page/checkbox_Event Item'))

WebUI.setText(findTestObject('Object Repository/Website/Checkout Page/input_Referral Code'), ReferralCode)

WebUI.click(findTestObject('Website/Checkout Page/button_Apply Referral'))

WebUI.click(findTestObject('Object Repository/Website/Checkout Page/button_Checkout'))

WebUI.delay(GlobalVariable.G_Timeout)

WebUI.click(findTestObject('Object Repository/Website/Checkout Page/radio_Payment Method'))

WebUI.click(findTestObject('Object Repository/Website/Checkout Page/button_Confirm'))

WebUI.delay(GlobalVariable.G_Timeout)

try {
    assert WebUI.verifyElementPresent(findTestObject('Object Repository/Website/Payment Page/button_Credit Debit card'), 
        GlobalVariable.G_Timeout, FailureHandling.OPTIONAL)

    WebUI.closeBrowser()
}
catch (AssertionError e) {
	
    WebUI.closeBrowser()

    throw e
} 


