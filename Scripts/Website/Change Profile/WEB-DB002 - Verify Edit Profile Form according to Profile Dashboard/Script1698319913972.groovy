import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.callTestCase(findTestCase('Website/Common Test Case/Login'), [('Email') : 'wovibas866@tutoreve.com', ('Password') : '8lpLjiqfLedVhYvvw1LrPA=='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.G_ProfileURL)

if (true) {
    name_profile = WebUI.getText(findTestObject('Website/Dashboard Profile Page/label_Profile Name'))

    email_profile = WebUI.getText(findTestObject('Website/Dashboard Profile Page/label_Email'))

    phone_profile = WebUI.getText(findTestObject('Website/Dashboard Profile Page/label_Phone'))

    dob_profile = WebUI.getText(findTestObject('Website/Dashboard Profile Page/label_Birth Day'))
}

WebUI.click(findTestObject('Website/Dashboard Profile Page/button_Edit Profile'))

if (true) {
    name_form = WebUI.getAttribute(findTestObject('Website/Change Profile Page/input_Fullname'), 'value')

    email_form = WebUI.getAttribute(findTestObject('Website/Change Profile Page/input_Email'), 'value')

    phone_form = WebUI.getAttribute(findTestObject('Website/Change Profile Page/input_Phone'), 'value')

    dob_form = WebUI.getAttribute(findTestObject('Website/Change Profile Page/input_BirthDay'), 'value')
}

WebUI.verifyEqual(name_profile, name_form)

WebUI.verifyEqual(email_profile, email_form)

WebUI.verifyEqual(phone_profile, phone_form)

WebUI.verifyEqual(dob_profile, dob_form)

WebUI.closeBrowser()

