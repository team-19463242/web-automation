import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.callTestCase(findTestCase('Website/Common Test Case/Login'), [('Email') : 'wovibas866@tutoreve.com', ('Password') : '8lpLjiqfLedVhYvvw1LrPA=='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.G_ProfileURL)

WebUI.verifyMatch('Putra Negara', WebUI.getText(findTestObject('Website/Dashboard Profile Page/label_Profile Name')), false)

WebUI.verifyMatch('wovibas866@tutoreve.com', WebUI.getText(findTestObject('Website/Dashboard Profile Page/label_Email')), 
    false)

WebUI.verifyMatch('08122600960', WebUI.getText(findTestObject('Website/Dashboard Profile Page/label_Phone')), false)

WebUI.verifyMatch('21-Jun-1961', WebUI.getText(findTestObject('Website/Dashboard Profile Page/label_Birth Day')), false)

WebUI.closeBrowser()

