import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.callTestCase(findTestCase('Website/Common Test Case/Login'), [('Email') : 'liknosospe@gufum.com', ('Password') : '8lpLjiqfLedVhYvvw1LrPA=='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.G_ProfileURL)

WebUI.click(findTestObject('Website/Dashboard Profile Page/button_Edit Profile'))

img_form = WebUI.getAttribute(findTestObject('Website/Change Profile Page/img_Profile'), 'src')

WebUI.uploadFile(findTestObject('Website/Change Profile Page/input_Profile Picture'), 'C:\\Users\\efendi\\git\\web-automation\\Source Data Files\\nadiemnew.png')

WebUI.verifyNotEqual(img_form, WebUI.getAttribute(findTestObject('Website/Change Profile Page/img_Profile'), 'src'))

WebUI.closeBrowser()

