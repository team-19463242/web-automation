import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.G_AppPATH, false)

Mobile.tap(findTestObject('Object Repository/Mobile/Home Page/button_Login Here'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Login Page/button_Register now'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Register Page/input_Nama'), 'Putra Bangsa', 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/input_Tanggal Lahir'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/input_Conf_Tanggal Lahir'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Register Page/input_Email'), 'nugraha.fajar.1227@gmail.com', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Register Page/input_Whatsapp'), '123456789', 0)

Mobile.setEncryptedText(findTestObject('Object Repository/Mobile/Register Page/input_Kata Sandi'), 'cZFrDSk31FeaspcjiMwZ6g==',
	0)

Mobile.setEncryptedText(findTestObject('Mobile/Register Page/input_Conf_Kata Sandi'), 'cZFrDSk31FeaspcjiMwZ6g==', 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/checkbox_I Agree'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Register Page/button_Daftar'), 0)

Mobile.verifyElementVisible(findTestObject('Mobile/Register Page/Validation/text_Email sudah terdaftar'), 0)

Mobile.closeApplication()