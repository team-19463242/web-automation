<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Whatsapp_whatsapp</name>
   <tag></tag>
   <elementGuidId>7d643d4d-4377-4c15-a86b-34ff2995bc2c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#whatsapp</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='whatsapp']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>78988884-eb61-4984-bbb3-10319465d861</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>a8dc8428-70b2-4e84-bba4-1457f2c5172d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>number</value>
      <webElementGuid>8ea8d48f-3421-417c-b491-7643a3097b0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>WhatsApp </value>
      <webElementGuid>df01f14f-49cb-4015-92da-78f14db02214</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>8d667051-aa92-4ef0-9cf2-cdea606fa03d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>e286b82d-e695-4e85-a0ba-7de240e933b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>0fa748d5-a40c-4fd3-bd77-251346c718fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;whatsapp&quot;)</value>
      <webElementGuid>52e3ef06-5968-4167-bdf5-d99ae66e9636</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='whatsapp']</value>
      <webElementGuid>1b68235f-95a8-43c9-bfbc-96a6d4242b18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/input</value>
      <webElementGuid>92fcfffc-46d0-4e62-bf48-4f84e25a5d24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'whatsapp' and @type = 'number' and @placeholder = 'WhatsApp ' and @name = 'whatsapp']</value>
      <webElementGuid>f1209558-7418-430f-8242-1c45b07983ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
