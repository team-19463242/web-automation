<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>API-UPR002_Empty Name</name>
   <tag></tag>
   <elementGuidId>2960a513-0930-4229-8ee7-db9b2f6fe29e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZjRmZDRmZTBhZWVjNTAyODljZjkxYzJhNzY2MzYzOGJhOTE3MzRkM2MxMmQxYWRiNzAyNWJjMWU1OTJkZWE1MGZiMDM3YzI2MmIwY2MxODciLCJpYXQiOjE2OTgxMjY0MjIuNTgyNDk3LCJuYmYiOjE2OTgxMjY0MjIuNTgyNTAyLCJleHAiOjE3Mjk3NDg4MjIuNTgwNDAzLCJzdWIiOiIxMjIxIiwic2NvcGVzIjpbXX0.edRR_LqcFlGLv7iVuC7GWSFEgX_IrTeg3ErHvrfRDnz3yjxARuPZLeDwKk3hMDXpWBJfH0XYJlIGb0cd0zit0elHdCU3vxLm9ahjals39TgagT745rMlthxCe6Pq7mQfbi3OOI5Ww8BcK_7opOxGSwFP_AUAzWH4wnL_Xw1TOhZms2BUxAd_jST1zFThnQm09v940MAfKi_NgNv0rWmcYapsRb24ReRHe6rq6JZ5GexGeKxKiTjSIVUtUFmVgAvxBxrDCr0HieIgXa_3rH0OO179W5YN-0GZUUuz-Lhkdfb45MhqI5MWEtmBG1NJYWl9qqAAM_H3aAtWFEV3NrGwj2PhYXmLSotRpT3mG7HvUu5_4vGpxOYo8_FTP9Od8FFvsapaWkcaUh8C65klxSsFkCdkNpc7_uh_oTXnYecdhfqIPjgjLjj9mLKM-lr_ueCIeovDwWF802Onx1fXniJPoIO4cSFsL_RqYhCD8qu3Wabc6OYLskrSaRIUjST4leggZypob3_jsp8QaH0OwAuvbclZ5QSPenM7lPlgP1YWJNHDgZYfqqnCHo4bXR7_fgIYI2XcTQzPjJIW3D4D0Og-__xOZx0v3zRRFNF_VoiAk2liY-Xqwxvl7vD8HGfx7yy9RJiLC_3bnjVC_GLuIQ3IfaGZWdZBUUi7dOKSGmFzVV0</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;0812123412&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;1996-03-07&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;Source Data Files/fotoprofile.jpg&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>772a5ded-524e-401d-b682-d5e2a72cc732</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZjRmZDRmZTBhZWVjNTAyODljZjkxYzJhNzY2MzYzOGJhOTE3MzRkM2MxMmQxYWRiNzAyNWJjMWU1OTJkZWE1MGZiMDM3YzI2MmIwY2MxODciLCJpYXQiOjE2OTgxMjY0MjIuNTgyNDk3LCJuYmYiOjE2OTgxMjY0MjIuNTgyNTAyLCJleHAiOjE3Mjk3NDg4MjIuNTgwNDAzLCJzdWIiOiIxMjIxIiwic2NvcGVzIjpbXX0.edRR_LqcFlGLv7iVuC7GWSFEgX_IrTeg3ErHvrfRDnz3yjxARuPZLeDwKk3hMDXpWBJfH0XYJlIGb0cd0zit0elHdCU3vxLm9ahjals39TgagT745rMlthxCe6Pq7mQfbi3OOI5Ww8BcK_7opOxGSwFP_AUAzWH4wnL_Xw1TOhZms2BUxAd_jST1zFThnQm09v940MAfKi_NgNv0rWmcYapsRb24ReRHe6rq6JZ5GexGeKxKiTjSIVUtUFmVgAvxBxrDCr0HieIgXa_3rH0OO179W5YN-0GZUUuz-Lhkdfb45MhqI5MWEtmBG1NJYWl9qqAAM_H3aAtWFEV3NrGwj2PhYXmLSotRpT3mG7HvUu5_4vGpxOYo8_FTP9Od8FFvsapaWkcaUh8C65klxSsFkCdkNpc7_uh_oTXnYecdhfqIPjgjLjj9mLKM-lr_ueCIeovDwWF802Onx1fXniJPoIO4cSFsL_RqYhCD8qu3Wabc6OYLskrSaRIUjST4leggZypob3_jsp8QaH0OwAuvbclZ5QSPenM7lPlgP1YWJNHDgZYfqqnCHo4bXR7_fgIYI2XcTQzPjJIW3D4D0Og-__xOZx0v3zRRFNF_VoiAk2liY-Xqwxvl7vD8HGfx7yy9RJiLC_3bnjVC_GLuIQ3IfaGZWdZBUUi7dOKSGmFzVV0</value>
      <webElementGuid>772ed366-926f-47d3-afec-3e029043105c</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.site/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
