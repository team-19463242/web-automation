<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Edit Profile</name>
   <tag></tag>
   <elementGuidId>384a37e5-3194-4b36-b1b2-128977d0bb48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='col-lg-4 col-md-4 col-sm-12 text-center']/div[@class=&quot;text-white mb-3&quot;]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.text-white.mb-3 > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5c8742c4-f0e6-41e1-9671-86f7d9d9a082</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/dashboard/profile/edit</value>
      <webElementGuid>ed542900-258c-47dc-bfcd-faacc3d58e61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Edit Profile </value>
      <webElementGuid>f0bef888-a981-4cba-9df5-3f604ee21ad9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row mt-4 align-items-end&quot;]/div[@class=&quot;col-lg-4 col-md-4 col-sm-12 text-center&quot;]/div[@class=&quot;text-white mb-3&quot;]/a[1]</value>
      <webElementGuid>b02d9ca5-5525-466e-bec7-a6aab9f8e759</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div[3]/div/a</value>
      <webElementGuid>b59f3b72-7817-48bf-b3f4-f74f8ab72bf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Edit Profile')]</value>
      <webElementGuid>741bb769-f072-4758-96bb-2a4241be5bdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='content_copy'])[1]/following::a[1]</value>
      <webElementGuid>065f7f0a-853e-476a-b9de-30ebdab7b30d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/preceding::a[1]</value>
      <webElementGuid>3b4fcae0-0f11-4b1e-b592-eb70d9a1f6ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detail Information'])[1]/preceding::a[2]</value>
      <webElementGuid>349a1c69-fa5e-46e5-8ef1-04fe4ed276cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Edit Profile']/parent::*</value>
      <webElementGuid>acb2db35-62ff-4106-8887-6a89753c1d9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/dashboard/profile/edit')]</value>
      <webElementGuid>ac03c1e4-b528-4775-8cc2-c8be76e803b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/a</value>
      <webElementGuid>650a8f81-d445-4000-8f5c-3c2fbe53a35c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/dashboard/profile/edit' and (text() = 'Edit Profile ' or . = 'Edit Profile ')]</value>
      <webElementGuid>1abde195-cade-4646-a631-a49a69c2a489</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
