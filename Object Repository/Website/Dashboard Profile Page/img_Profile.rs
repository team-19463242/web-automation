<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Profile</name>
   <tag></tag>
   <elementGuidId>d5de4d90-b0c4-407f-b00c-ab7eef846e95</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;col-lg-3 col-md-4 col-sm-12&quot;]/img[@alt=&quot;image&quot; and @class=&quot;rounded-circle author-box-picture&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
