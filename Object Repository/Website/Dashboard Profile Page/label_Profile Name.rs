<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Profile Name</name>
   <tag></tag>
   <elementGuidId>d846095f-d293-4e2e-ad31-a1d9d3900109</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='col-lg-5 col-md-4 col-sm-12 pt-4 pl-0 text-center']/h2[@class=&quot;text-white&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h2.text-white</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>a1301973-4661-4e6f-afee-8ac94fb9c121</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-white</value>
      <webElementGuid>0affc91d-f3b8-4a5f-90df-3e34991fb56d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Budi Gunawan</value>
      <webElementGuid>62e4cafe-eb71-46ac-a437-713ab6282a7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row mt-4 align-items-end&quot;]/div[@class=&quot;col-lg-5 col-md-4 col-sm-12 pt-4 pl-0 text-center&quot;]/h2[@class=&quot;text-white&quot;]</value>
      <webElementGuid>2a45fe08-82ec-4bdd-9a74-db76239cb324</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div[2]/h2</value>
      <webElementGuid>51e542c1-153b-405e-8bf0-b1746f806363</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profile'])[1]/following::h2[1]</value>
      <webElementGuid>ece53bcd-fb80-4978-b595-f6524048c5e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::h2[1]</value>
      <webElementGuid>f94b4b9d-46bd-4665-aba1-d943403ba5fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='content_copy'])[1]/preceding::h2[1]</value>
      <webElementGuid>acb8f239-1401-4b78-b922-5ea1b0bc3994</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Budi Gunawan']/parent::*</value>
      <webElementGuid>c2786623-7851-42d5-9884-3191ab5beee6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>de8f6f24-3d8c-4b7c-a948-3f5df076ba16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = ' Budi Gunawan' or . = ' Budi Gunawan')]</value>
      <webElementGuid>5174a2e0-2c14-4ce2-a817-89369d3747dd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
