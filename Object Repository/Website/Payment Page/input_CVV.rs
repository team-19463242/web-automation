<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_CVV</name>
   <tag></tag>
   <elementGuidId>1fee2c7e-ccfe-4f30-8109-8f37dd4962e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[ @id=&quot;card-cvv&quot; and @autocomplete=&quot;cc-csc&quot; and @placeholder=&quot;123&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#card-cvv</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>11f86919-e9e5-491c-a15f-45c37a05fc5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>card-cvv</value>
      <webElementGuid>97e6c863-dde5-4f84-8006-1fd703acd9a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>579c1a2e-f610-4806-8d7f-2bd987c702d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>cc-csc</value>
      <webElementGuid>6b23c2bb-fe2c-480d-99b3-4a3cd30b8419</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>057e2211-5acd-46ed-8415-9eb5807fcab6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>8d5f46b2-a74f-4812-89bc-a9b95cfeb180</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-input-value</value>
      <webElementGuid>3b58309d-78e1-4e86-bb2b-bf1670598adb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>d7d04aa7-afb9-42e7-adf9-3a9520c13fe7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;card-cvv&quot;)</value>
      <webElementGuid>9fe13d17-e58e-41ca-88e4-4d38ea65c47a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Payment Page/iframe_midtrans</value>
      <webElementGuid>a8610d5b-5426-4a7f-9bb1-fc7fe38f2fe7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='card-cvv']</value>
      <webElementGuid>f8b5ab2c-9794-4392-a79a-9ba3c6ce83f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div[3]/div[2]/input</value>
      <webElementGuid>f35895c2-bc42-4dba-80b0-5603c381129a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/input</value>
      <webElementGuid>d37f2124-a580-4c17-9057-96526f0a0e0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'card-cvv' and @type = 'tel']</value>
      <webElementGuid>644240a7-12f0-4d57-9b53-01ba3fb744e8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
