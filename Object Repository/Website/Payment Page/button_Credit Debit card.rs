<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Credit Debit card</name>
   <tag></tag>
   <elementGuidId>3c72307a-d5ae-40ee-9808-3d9f7953cb2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;list&quot; and @data-testid=&quot;list-item&quot; and @href=&quot;#/credit-card&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.list-title.text-actionable-bold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3e04bf4e-089d-44d7-b50a-f08b90eebfa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list-title text-actionable-bold</value>
      <webElementGuid>56c729ce-f320-4f69-bfd8-6e9de82b3250</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Credit/debit card</value>
      <webElementGuid>e0bf1502-8880-4d74-b135-8a1aea68717c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;payment-container-list&quot;]/div[3]/a[@class=&quot;list&quot;]/div[@class=&quot;list-content&quot;]/div[@class=&quot;list-title text-actionable-bold&quot;]</value>
      <webElementGuid>c6d0f441-34de-49aa-8309-0f0694773689</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Payment Page/iframe_midtrans</value>
      <webElementGuid>8cb20d7c-63a6-40f2-b3a8-55a2adc6a124</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div[3]/a/div/div</value>
      <webElementGuid>a7bba21d-4883-4e41-b51d-10385b5c5a7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other banks'])[1]/following::div[4]</value>
      <webElementGuid>8f89a9c7-68e3-4262-95dd-e25625d18a23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Virtual account'])[1]/following::div[32]</value>
      <webElementGuid>a144fac9-0613-466e-9881-c3cfcd775ab0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ShopeePay'])[1]/preceding::div[8]</value>
      <webElementGuid>c9e206bc-c020-486a-b421-81dce1227a30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret'])[1]/preceding::div[16]</value>
      <webElementGuid>e59ccf5f-2724-4b4e-b55c-ae94f555a75d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Credit/debit card']/parent::*</value>
      <webElementGuid>dfa1320c-204b-4c9e-b637-497144da8e3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a/div/div</value>
      <webElementGuid>3037d233-cca0-4164-b432-516b6b28e499</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Credit/debit card' or . = 'Credit/debit card')]</value>
      <webElementGuid>0036441e-3f21-40da-9161-6709bae9bc97</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
