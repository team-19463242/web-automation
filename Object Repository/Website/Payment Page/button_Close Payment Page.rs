<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Close Payment Page</name>
   <tag></tag>
   <elementGuidId>325e0e6d-3ae1-433f-8053-2c74ead65a85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='close-snap-button clickable']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.close-snap-button.clickable</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>16f00309-8b16-4120-9852-804e9b7c1c3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>close-snap-button clickable</value>
      <webElementGuid>adcc97bc-3a2e-47f6-b495-81d04afd1576</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header&quot;)/div[@class=&quot;title-bar&quot;]/div[@class=&quot;close-snap-button clickable&quot;]</value>
      <webElementGuid>63420fde-a935-4826-a0fd-9a622e4baf36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Website/Payment Page/iframe_midtrans</value>
      <webElementGuid>e831eb45-f571-4b61-9bb0-7ecfddfb684a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='header']/div/div[2]</value>
      <webElementGuid>81fcacab-3036-4853-8104-f1ed7c0464bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PT Dwidata Talenta Prima'])[1]/following::div[1]</value>
      <webElementGuid>7172ce32-e89e-4e96-9180-097c41829620</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TEST'])[1]/following::div[6]</value>
      <webElementGuid>861b47bd-0a62-4050-ab20-178d60524f31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/preceding::div[1]</value>
      <webElementGuid>ba145cde-632f-4bdc-bccd-ce46907c5dc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]</value>
      <webElementGuid>f29609f7-a9b2-4728-b48b-b2ea2dcf241b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
