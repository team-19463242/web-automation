<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_Referal or Voucher Not Found</name>
   <tag></tag>
   <elementGuidId>87c8f9b8-d1b9-4e01-8eba-cf3b4a91e001</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//small[@id='error_message']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#error_message</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>small</value>
      <webElementGuid>9d5bdcee-8fc2-41bd-acf4-9d1fb05374d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>error_message</value>
      <webElementGuid>a312c57a-49ac-4b3d-b3b0-6871e5828b23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Referal or Voucher Not Found</value>
      <webElementGuid>4ab4c875-3a41-43b2-a8f0-363ccdf44ce6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;error_message&quot;)</value>
      <webElementGuid>5b027bc0-1907-4984-9e39-c5aded56c0c5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//small[@id='error_message']</value>
      <webElementGuid>fa023afd-0833-4067-8538-33995ab33685</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div[2]/small</value>
      <webElementGuid>bbedb9ca-5933-4a6a-a5b9-76378214f3dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::small[1]</value>
      <webElementGuid>a07d157e-8cff-4749-abc7-0ca72692ea3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[4]/following::small[1]</value>
      <webElementGuid>18a5b994-f58c-4072-a47c-1715eedd1734</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Pembelian'])[1]/preceding::small[2]</value>
      <webElementGuid>2395ba7a-362c-490f-b8bc-7406a455e72b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVN22102300014'])[1]/preceding::small[2]</value>
      <webElementGuid>f9250ca9-0dfb-4326-8283-8940b53d1891</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Referal or Voucher Not Found']/parent::*</value>
      <webElementGuid>6f2101d0-b8c8-4ae3-be23-c40ce1075095</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small</value>
      <webElementGuid>f3766e2a-12fa-4e50-8791-4f902e42d339</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//small[@id = 'error_message' and (text() = 'Referal or Voucher Not Found' or . = 'Referal or Voucher Not Found')]</value>
      <webElementGuid>f9fd1ce0-941e-4335-9bd9-50040c89bcdb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
