<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Unpaid</name>
   <tag></tag>
   <elementGuidId>9bb31e75-2395-45dc-b2a1-26459bb2a558</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='badge-status']/h5/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.badge.badge-warning</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>835fa35a-9638-490b-a670-8c9d3332e266</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>badge badge-warning</value>
      <webElementGuid>c9a23999-d11d-47d3-8cca-2e3bd4209593</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Unpaid</value>
      <webElementGuid>ad9f8aef-dbcb-46ab-b5df-8f0c4adab5a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;badge-status&quot;)/h5[1]/span[@class=&quot;badge badge-warning&quot;]</value>
      <webElementGuid>2504d3b0-5bc1-4c79-8b7b-da9e5f9aa6fa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='badge-status']/h5/span</value>
      <webElementGuid>45dd326f-6776-4f14-a0e6-70f030189c6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='#EVN22102300035'])[1]/following::span[1]</value>
      <webElementGuid>53971735-77dd-4947-81ed-1aa480036fb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/following::span[1]</value>
      <webElementGuid>104e7f9b-a4d7-4901-a8d5-fcc8e9034bc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bill To :'])[1]/preceding::span[1]</value>
      <webElementGuid>472d615d-0b80-4c84-a23f-9df82f136a3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Putra Bangsa'])[1]/preceding::span[1]</value>
      <webElementGuid>e901a283-e451-48a7-800d-e1ba3a181115</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Unpaid']/parent::*</value>
      <webElementGuid>5567e21f-5d34-4317-9986-51cef6f098c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5/span</value>
      <webElementGuid>df2f3889-833c-48ac-8bad-972d0aba119d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Unpaid' or . = 'Unpaid')]</value>
      <webElementGuid>d6fad66b-b3cb-43c9-a348-1b4ac03887b3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
