<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Web_Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e62320f8-ec7a-480d-b074-4da39e90415f</testSuiteGuid>
   <testCaseLink>
      <guid>608609f5-2119-4064-8cac-8d1bb5300afe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Login/WEB-LOG001 - Login with Valid Credentials</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2cb3d091-2c18-4b70-b381-5ce9928c94c3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TD_Login_Valid Credentials</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>2cb3d091-2c18-4b70-b381-5ce9928c94c3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>1134c0a4-1cb3-4a00-b084-06137ea5bbc3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2cb3d091-2c18-4b70-b381-5ce9928c94c3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>daabae3a-6ce3-457b-b0ff-4704109e70bd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2cb39327-3745-4f72-a536-06b20f1c1e03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Login/WEB-LOG002 - Login with Invalid Credentials</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7097002e-eb12-4b1d-9afe-0044dd0ea515</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TD_Login_Invalid Credentials</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>7097002e-eb12-4b1d-9afe-0044dd0ea515</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>98f9e309-2da5-497b-b6e3-9bceb683d668</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7097002e-eb12-4b1d-9afe-0044dd0ea515</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>c28e368f-f73e-460c-a60d-11fb20ddc3ea</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8c86bc74-8d17-447e-a241-b33bb7108c08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Login/WEB-LOG003 - Login with Empty Credentials</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4cd671cb-0543-419b-9126-d3b5806e325d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TD_Login_Empty Credentials</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>4cd671cb-0543-419b-9126-d3b5806e325d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>1ffcf4c5-4dc1-4f10-88bf-d9dc004240c7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4cd671cb-0543-419b-9126-d3b5806e325d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>dc4eb6ca-1100-425c-bac9-efd9e3235915</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c4d3b10a-c05b-4529-8c53-0096f5c842aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Login/WEB-LOG004 - Login with Invalid Email Format</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>928b0cba-305c-4d2d-bd72-6edf7b4ea4ce</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TD_Login_Invalid Email Format</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>928b0cba-305c-4d2d-bd72-6edf7b4ea4ce</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>49c8054a-73d3-419d-b3bd-985863a50b14</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>928b0cba-305c-4d2d-bd72-6edf7b4ea4ce</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>f37e8508-8361-4197-8caa-a4c926cc35f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f0178216-6733-4e83-b910-02f63e197501</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Login/WEB-LOG005 - Login with registered username as email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>be3f6f3e-ebf0-4e71-97e4-de1dd979a8ed</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5e6066c7-5577-4344-a8a7-6ce761dc749d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e05ec1a0-02aa-4ff3-9d3d-70cc6600f767</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Login/WEB-LOG006 - Login with registered WA Number as email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>75c07d1d-0765-4971-ade7-d1ed49900be7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a0fb5a0d-58df-4a3b-b44d-dce7b7e5934e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>554f0743-af02-4d11-acb0-f737fe2484c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Login/WEB-LOG007 - Login with unregistered email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1eed8f1e-6e05-4658-8805-3fa16b6f327c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dd30ba9f-d4fd-4adf-a9ba-77b5cc0fdb5d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
