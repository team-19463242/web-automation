<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Web_Payment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>8023cc1e-b5e4-43b0-bd80-186c6cd52143</testSuiteGuid>
   <testCaseLink>
      <guid>510ef0ac-b514-461e-baf0-92a85f87e891</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Payment/WEB-PY004 - Apply invalid referral code</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d5e8d9ba-4eec-4e41-8e8d-f1a55a232d8d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bd253528-a593-4a76-81dd-5d094405dd96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Payment/WEB-PY005 - Continue Checkout with invalid referral code</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e4ba0b49-529f-48c6-a7f6-7b567d08c6b4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4eaac473-3cae-41b8-9b06-0535cf862678</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Payment/WEB-PY006 - Checkout without check the event</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>835dd274-2637-4602-9246-f658a9f7d9cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Payment/WEB-PY007 - Remove event on checkout</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ccf05f4d-9469-494d-a426-25c38416aa23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/Payment/WEB-PY008 - Checkout with Failed Payment Status</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
